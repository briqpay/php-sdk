# Briqpay

This is the API documentation for Briqpay. You can find out more about
us and our offering at our website [https://briqpay.com](https://briqpay.com)
In order to get credentials to the playgrund API  Please register at [https://app.briqpay.com](https://app.briqpay.com)
# Introduction 
Briqpay Checkout is an inline checkout solution for your b2b ecommerce.
 Briqpay Checkout gives you the flexibility of controlling your payment methods and credit rules while optimizing the UX for your customers
# SDKs
Briqpay offers standard SDKs to PHP and .NET based on these swagger definitions. You can download them respively or use our swagger defintitions to codegen your own versions.
#### For .NET
``
Install-Package Briqpay
``
#### For PHP
``
composer require briqpay/php-sdk 
``
# Standard use-case
As a first step of integration you will need to create a checkout session. \\n\\nIn this session you provide Briqpay with the basic information necessary.
In the response from briqpay you will recieve a htmlsnippet that is to be inserted into your frontend.
The snippet provided by briqpay will render an iframe where the user will complete the purchase.
Once completed, briqpay will redirect the customer to a confirmation page that you have defined.

  ![alt](https://cdn.briqpay.com/static/developer-portal/checkout-integration.png)

# JavaScript SDK
The first step of integration is to add our JS to your site just before closing the  ``<head>`` tag. This ensures that our JS library is avaliable to load the checkout.

``<script src=\"https://api.briqpay.com/briq.min.js\"></script>``

Briqpay offers a few methods avaliable through our Javascript SDK. The library is added by our iframe and is avalable on ``window._briqpay``
If you offer the posibility to update the cart or order amonts on the checkout page, the JS library will help you.

If your store charges the customer different costs and fees depening on their shipping location, you can listen to the ``addressupdate``event in order to re-calculate the total cost.
```javascript
    window._briqpay.subscribe('addressupdate', function (data) {
  console.log(data)
    })
```

If your frontend needs to perform an action whe the signup has completed, listen to the ``signup_finalized`` event.
```javascript
    window._briqpay.subscribe('signup_finalized', function (status) {
      // redirect or handle status 'success' / 'failure'
    })
```

If you allow customers to change the total cart value, you can utilise the JS library to suspend the iframe while you perform a backen update call towards our services. As described below:
![alt](https://cdn.briqpay.com/static/developer-portal/suspend-resume.png)
The iframe will auto-resume after 7 seconds if you dont call ``_briqpay.resume()`` before

# Test Data
In order to verify your integration you will neeed to use test data towards our credit engine.

## Company identication numbers
* 1111111111 - To recieve a high credit scoring company ( 100 in rating)
* 2222222222 - To test the enviournment with a bad credit scoring company (10 in rating)

## Card details
In our playground setup your account is by default setup with a Stripe integration.
In order to test out the card form you can use the below card numbers:

* 4000002500003155 - To mock 3ds authentication window
* 4000000000000069 Charge is declined with an expired_card code.

You can use any valid expiry and CVC code

# Authentication 
Briqpay utilizes JWT in order to authenticate calls to our platform. 
Authentication tokens expire after 48 hours, and at that point you can generate a new token for the given resource using the ``/auth`` endpoint.
 - Basic Auth - only used on the auth endpoint in order to get the Bearer Token
 - JWT Bearer Token - All calls towards the API utlizes this method\"



## Installation & Usage

### Requirements

PHP 7.3 and later.
Should also work with PHP 8.0 but has not been tested.

### Composer

To install the bindings via [Composer](https://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://github.com/GIT_USER_ID/php-sdk.git"
    }
  ],
  "require": {
    "GIT_USER_ID/php-sdk": "*@dev"
  }
}
```

Then run `composer install`

### Manual Installation

Download the files and include `autoload.php`:

```php
<?php
require_once('/path/to/Briqpay/vendor/autoload.php');
```

## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



// Configure HTTP basic authorization: basicAuth
$config = Briqpay\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new Briqpay\Api\AuthenticationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getAuthToken();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling AuthenticationApi->getAuthToken: ', $e->getMessage(), PHP_EOL;
}

```

## API Endpoints

All URIs are relative to *https://playground-api.briqpay.com*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AuthenticationApi* | [**getAuthToken**](docs/Api/AuthenticationApi.md#getauthtoken) | **GET** /auth | Generate bearer token
*AuthenticationApi* | [**getAuthTokenForSession**](docs/Api/AuthenticationApi.md#getauthtokenforsession) | **GET** /auth/{sessionid} | Generate a new auth token for session
*AuthenticationApi* | [**getSignupSessionAuth**](docs/Api/AuthenticationApi.md#getsignupsessionauth) | **GET** /auth/signup/{sessionid} | Get a new auth token for a signup session
*CheckoutApi* | [**cancelRecurringToken**](docs/Api/CheckoutApi.md#cancelrecurringtoken) | **POST** /checkout/v1/recurring/token/cancel | Cancel a recurring token
*CheckoutApi* | [**createSession**](docs/Api/CheckoutApi.md#createsession) | **POST** /checkout/v1/sessions | Create a payment session
*CheckoutApi* | [**patchCheckoutSession**](docs/Api/CheckoutApi.md#patchcheckoutsession) | **POST** /checkout/v1/sessions/patch | Patch a session
*CheckoutApi* | [**purchaseDecision**](docs/Api/CheckoutApi.md#purchasedecision) | **POST** /checkout/v2/sessions/{sessionid}/decision/purchase | Approve or deny a purchase a session
*CheckoutApi* | [**readRecurringToken**](docs/Api/CheckoutApi.md#readrecurringtoken) | **POST** /checkout/v1/recurring/token | Read a recurring token
*CheckoutApi* | [**readSession**](docs/Api/CheckoutApi.md#readsession) | **POST** /checkout/v1/readsession | Read session
*CheckoutApi* | [**updateSession**](docs/Api/CheckoutApi.md#updatesession) | **POST** /checkout/v1/sessions/update | Update a session
*CheckoutApi* | [**yourchargewebhookPost**](docs/Api/CheckoutApi.md#yourchargewebhookpost) | **POST** /yourchargewebhook | Charge result
*HostedPageApi* | [**createHostedPage**](docs/Api/HostedPageApi.md#createhostedpage) | **POST** /hostedpage/v1/checkout | Create a hosted checkout page
*OrderManagementApi* | [**captureOrder**](docs/Api/OrderManagementApi.md#captureorder) | **POST** /order-management/v1/capture-order | Capture a placed order
*OrderManagementApi* | [**refundOrder**](docs/Api/OrderManagementApi.md#refundorder) | **POST** /order-management/v1/refund-order | Refunding a captured
*SignupApi* | [**createSignup**](docs/Api/SignupApi.md#createsignup) | **POST** /signup/v1/session | Create a new signup session
*SignupApi* | [**readSignup**](docs/Api/SignupApi.md#readsignup) | **GET** /signup/v1/session | Read an ongoing signup session
*SignupApi* | [**sendValidateResponse**](docs/Api/SignupApi.md#sendvalidateresponse) | **POST** /signup/v1/hooks/validated | Validation Result
*SignupApi* | [**yoursignupwebhookPost**](docs/Api/SignupApi.md#yoursignupwebhookpost) | **POST** /yoursignupwebhook | SignupWebhook Request

## Models

- [Address](docs/Model/Address.md)
- [AuthenticationResponse](docs/Model/AuthenticationResponse.md)
- [BillingConfiguration](docs/Model/BillingConfiguration.md)
- [BriqpayCreateHPPSessionRequest](docs/Model/BriqpayCreateHPPSessionRequest.md)
- [BriqpayCreateHPPSessionRequestConfig](docs/Model/BriqpayCreateHPPSessionRequestConfig.md)
- [BriqpayCreateHPPSessionRequestDeliverymethod](docs/Model/BriqpayCreateHPPSessionRequestDeliverymethod.md)
- [BriqpayValidateRequest](docs/Model/BriqpayValidateRequest.md)
- [CancelRecurringTokenRequest](docs/Model/CancelRecurringTokenRequest.md)
- [CaptureResponse](docs/Model/CaptureResponse.md)
- [CartItem](docs/Model/CartItem.md)
- [CreateCaptureRequest](docs/Model/CreateCaptureRequest.md)
- [CreateRefundRequest](docs/Model/CreateRefundRequest.md)
- [CreateSignupRequest](docs/Model/CreateSignupRequest.md)
- [CreateSignupRequestMerchanturls](docs/Model/CreateSignupRequestMerchanturls.md)
- [CreateSignupResponse](docs/Model/CreateSignupResponse.md)
- [CustomInput](docs/Model/CustomInput.md)
- [GetSessionRequest](docs/Model/GetSessionRequest.md)
- [IndividualRuleResult](docs/Model/IndividualRuleResult.md)
- [LockFieldsConfiguration](docs/Model/LockFieldsConfiguration.md)
- [LockType](docs/Model/LockType.md)
- [MerchantConfig](docs/Model/MerchantConfig.md)
- [MerchantPrefillAddress](docs/Model/MerchantPrefillAddress.md)
- [MerchantReference](docs/Model/MerchantReference.md)
- [MerchantUrls](docs/Model/MerchantUrls.md)
- [OrderNote](docs/Model/OrderNote.md)
- [OrderNoteConfiguration](docs/Model/OrderNoteConfiguration.md)
- [PSPRuleResults](docs/Model/PSPRuleResults.md)
- [PatchSessionRequest](docs/Model/PatchSessionRequest.md)
- [PaymentConfiguration](docs/Model/PaymentConfiguration.md)
- [PaymentConfigurationPspRulesOverride](docs/Model/PaymentConfigurationPspRulesOverride.md)
- [ProductType](docs/Model/ProductType.md)
- [PurchaseDecisionRequest](docs/Model/PurchaseDecisionRequest.md)
- [ReadRecurringToken](docs/Model/ReadRecurringToken.md)
- [ReadRecurringTokenRequest](docs/Model/ReadRecurringTokenRequest.md)
- [ReadSignupResponse](docs/Model/ReadSignupResponse.md)
- [ReadSignupResponseAddress](docs/Model/ReadSignupResponseAddress.md)
- [ReadSignupResponseMerchanturls](docs/Model/ReadSignupResponseMerchanturls.md)
- [ReadSignupResponseUser](docs/Model/ReadSignupResponseUser.md)
- [RecurringChargeResultRequest](docs/Model/RecurringChargeResultRequest.md)
- [RecurringSession](docs/Model/RecurringSession.md)
- [RefundResponse](docs/Model/RefundResponse.md)
- [RuleOverridePSP](docs/Model/RuleOverridePSP.md)
- [RuleOverridePSPHandles](docs/Model/RuleOverridePSPHandles.md)
- [Session](docs/Model/Session.md)
- [SessionPurchasepaymentmethod](docs/Model/SessionPurchasepaymentmethod.md)
- [SessionRequest](docs/Model/SessionRequest.md)
- [SessionRequestMerchantBilling](docs/Model/SessionRequestMerchantBilling.md)
- [SessionRequestMerchantShipping](docs/Model/SessionRequestMerchantShipping.md)
- [SessionRequestRecurring](docs/Model/SessionRequestRecurring.md)
- [SessionState](docs/Model/SessionState.md)
- [SessionTags](docs/Model/SessionTags.md)
- [ShippingConfiguration](docs/Model/ShippingConfiguration.md)
- [SignupAuthTokenResponse](docs/Model/SignupAuthTokenResponse.md)
- [UpdateSessionRequest](docs/Model/UpdateSessionRequest.md)
- [ValidateSignupResponse](docs/Model/ValidateSignupResponse.md)
- [ValidateSignupResponseError](docs/Model/ValidateSignupResponseError.md)

## Authorization

### basicAuth

- **Type**: HTTP basic authentication


### bearerAuth

- **Type**: Bearer authentication (JWT)

## Tests

To run the tests, use:

```bash
composer install
vendor/bin/phpunit
```

## Author

hello@briqpay.com

## About this package

This PHP package is automatically generated by the [OpenAPI Generator](https://openapi-generator.tech) project:

- API version: `1.0.0`
- Build package: `org.openapitools.codegen.languages.PhpClientCodegen`
