<?php
/**
 * AuthenticationResponse
 *
 * PHP version 7.3
 *
 * @category Class
 * @package  Briqpay
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 */

/**
 * BriqPay Api
 *
 * This is the API documentation for Briqpay. You can find out more about us and our offering at our website [https://briqpay.com](https://briqpay.com) In order to get credentials to the playgrund API  Please register at [https://app.briqpay.com](https://app.briqpay.com) # Introduction  Briqpay Checkout is an inline checkout solution for your b2b ecommerce.  Briqpay Checkout gives you the flexibility of controlling your payment methods and credit rules while optimizing the UX for your customers # SDKs Briqpay offers standard SDKs to PHP and .NET based on these swagger definitions. You can download them respively or use our swagger defintitions to codegen your own versions. #### For .NET `` Install-Package Briqpay `` #### For PHP `` composer require briqpay/php-sdk  `` # Standard use-case As a first step of integration you will need to create a checkout session. \\n\\nIn this session you provide Briqpay with the basic information necessary. In the response from briqpay you will recieve a htmlsnippet that is to be inserted into your frontend. The snippet provided by briqpay will render an iframe where the user will complete the purchase. Once completed, briqpay will redirect the customer to a confirmation page that you have defined.    ![alt](https://cdn.briqpay.com/static/developer-portal/checkout-integration.png)  # JavaScript SDK The first step of integration is to add our JS to your site just before closing the  ``<head>`` tag. This ensures that our JS library is avaliable to load the checkout.  ``<script src=\"https://api.briqpay.com/briq.min.js\"></script>``  Briqpay offers a few methods avaliable through our Javascript SDK. The library is added by our iframe and is avalable on ``window._briqpay`` If you offer the posibility to update the cart or order amonts on the checkout page, the JS library will help you.  If your store charges the customer different costs and fees depening on their shipping location, you can listen to the ``addressupdate``event in order to re-calculate the total cost. ```javascript     window._briqpay.subscribe('addressupdate', function (data) {   console.log(data)     }) ```  If your frontend needs to perform an action whe the signup has completed, listen to the ``signup_finalized`` event. ```javascript     window._briqpay.subscribe('signup_finalized', function (status) {       // redirect or handle status 'success' / 'failure'     }) ```  If you allow customers to change the total cart value, you can utilise the JS library to suspend the iframe while you perform a backen update call towards our services. As described below: ![alt](https://cdn.briqpay.com/static/developer-portal/suspend-resume.png) The iframe will auto-resume after 7 seconds if you dont call ``_briqpay.resume()`` before  # Test Data In order to verify your integration you will neeed to use test data towards our credit engine.  ## Company identication numbers * 1111111111 - To recieve a high credit scoring company ( 100 in rating) * 2222222222 - To test the enviournment with a bad credit scoring company (10 in rating)  ## Card details In our playground setup your account is by default setup with a Stripe integration. In order to test out the card form you can use the below card numbers:  * 4000002500003155 - To mock 3ds authentication window * 4000000000000069 Charge is declined with an expired_card code.  You can use any valid expiry and CVC code  # Authentication  Briqpay utilizes JWT in order to authenticate calls to our platform.  Authentication tokens expire after 48 hours, and at that point you can generate a new token for the given resource using the ``/auth`` endpoint.  - Basic Auth - only used on the auth endpoint in order to get the Bearer Token  - JWT Bearer Token - All calls towards the API utlizes this method\"
 *
 * The version of the OpenAPI document: 1.0.0
 * Contact: hello@briqpay.com
 * Generated by: https://openapi-generator.tech
 * OpenAPI Generator version: 5.4.0
 */

/**
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */

namespace Briqpay\Models;

use \ArrayAccess;
use \Briqpay\ObjectSerializer;

/**
 * AuthenticationResponse Class Doc Comment
 *
 * @category Class
 * @package  Briqpay
 * @author   OpenAPI Generator team
 * @link     https://openapi-generator.tech
 * @implements \ArrayAccess<TKey, TValue>
 * @template TKey int|null
 * @template TValue mixed|null
 */
class AuthenticationResponse implements ModelInterface, ArrayAccess, \JsonSerializable
{
    public const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $openAPIModelName = 'AuthenticationResponse';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $openAPITypes = [
        'token' => 'string'
    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      * @phpstan-var array<string, string|null>
      * @psalm-var array<string, string|null>
      */
    protected static $openAPIFormats = [
        'token' => null
    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPITypes()
    {
        return self::$openAPITypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function openAPIFormats()
    {
        return self::$openAPIFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'token' => 'token'
    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'token' => 'setToken'
    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'token' => 'getToken'
    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$openAPIModelName;
    }


    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['token'] = $data['token'] ?? null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets token
     *
     * @return string|null
     */
    public function getToken()
    {
        return $this->container['token'];
    }

    /**
     * Sets token
     *
     * @param string|null $token The Bearer token to use for all subsequent calls on this session
     *
     * @return self
     */
    public function setToken($token)
    {
        $this->container['token'] = $token;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed|null
     */
    public function offsetGet($offset)
    {
        return $this->container[$offset] ?? null;
    }

    /**
     * Sets value based on offset.
     *
     * @param int|null $offset Offset
     * @param mixed    $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Serializes the object to a value that can be serialized natively by json_encode().
     * @link https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed Returns data which can be serialized by json_encode(), which is a value
     * of any type other than a resource.
     */
    public function jsonSerialize()
    {
       return ObjectSerializer::sanitizeForSerialization($this);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        return json_encode(
            ObjectSerializer::sanitizeForSerialization($this),
            JSON_PRETTY_PRINT
        );
    }

    /**
     * Gets a header-safe presentation of the object
     *
     * @return string
     */
    public function toHeaderValue()
    {
        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}


