# Briqpay\HostedPageApi

All URIs are relative to https://playground-api.briqpay.com.

Method | HTTP request | Description
------------- | ------------- | -------------
[**createHostedPage()**](HostedPageApi.md#createHostedPage) | **POST** /hostedpage/v1/checkout | Create a hosted checkout page


## `createHostedPage()`

```php
createHostedPage($briqpay_create_hpp_session_request)
```

Create a hosted checkout page

A first step in creating a hpp session is actually createa a Checkout session. This will then be used for the Hostedpage as the checkout. ![Create HPP session](https://briqpay.com/wp-content/uploads/2021/05/Skärmavbild-2021-05-19-kl.-08.30.51.png)  In order to provide the customer with a paylink, you need to create a hostedpage and define the delivery method.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new Briqpay\Api\HostedPageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$briqpay_create_hpp_session_request = new \Briqpay\Models\BriqpayCreateHPPSessionRequest(); // \Briqpay\Models\BriqpayCreateHPPSessionRequest

try {
    $apiInstance->createHostedPage($briqpay_create_hpp_session_request);
} catch (Exception $e) {
    echo 'Exception when calling HostedPageApi->createHostedPage: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **briqpay_create_hpp_session_request** | [**\Briqpay\Models\BriqpayCreateHPPSessionRequest**](../Model/BriqpayCreateHPPSessionRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
