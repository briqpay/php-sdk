# Briqpay\CheckoutApi

All URIs are relative to https://playground-api.briqpay.com.

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancelRecurringToken()**](CheckoutApi.md#cancelRecurringToken) | **POST** /checkout/v1/recurring/token/cancel | Cancel a recurring token
[**createSession()**](CheckoutApi.md#createSession) | **POST** /checkout/v1/sessions | Create a payment session
[**patchCheckoutSession()**](CheckoutApi.md#patchCheckoutSession) | **POST** /checkout/v1/sessions/patch | Patch a session
[**purchaseDecision()**](CheckoutApi.md#purchaseDecision) | **POST** /checkout/v2/sessions/{sessionid}/decision/purchase | Approve or deny a purchase a session
[**readRecurringToken()**](CheckoutApi.md#readRecurringToken) | **POST** /checkout/v1/recurring/token | Read a recurring token
[**readSession()**](CheckoutApi.md#readSession) | **POST** /checkout/v1/readsession | Read session
[**updateSession()**](CheckoutApi.md#updateSession) | **POST** /checkout/v1/sessions/update | Update a session
[**yourchargewebhookPost()**](CheckoutApi.md#yourchargewebhookPost) | **POST** /yourchargewebhook | Charge result


## `cancelRecurringToken()`

```php
cancelRecurringToken($cancel_recurring_token_request)
```

Cancel a recurring token

In order to cancel all future charges on this token, you need to cancel it

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = Briqpay\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$cancel_recurring_token_request = new \Briqpay\Models\CancelRecurringTokenRequest(); // \Briqpay\Models\CancelRecurringTokenRequest

try {
    $apiInstance->cancelRecurringToken($cancel_recurring_token_request);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->cancelRecurringToken: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **cancel_recurring_token_request** | [**\Briqpay\Models\CancelRecurringTokenRequest**](../Model/CancelRecurringTokenRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `createSession()`

```php
createSession($session_request): \Briqpay\Models\Session
```

Create a payment session

Once your customer has entered your checkout. You should create the payment session towards briqpay. The response will include the snippet needed to render our checkout iframe. If your customer is already logged into your systems you are able to provide briqpay with billing and shipping details at this point.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = Briqpay\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$session_request = new \Briqpay\Models\SessionRequest(); // \Briqpay\Models\SessionRequest | Mandatory method to create a payment session

try {
    $result = $apiInstance->createSession($session_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->createSession: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **session_request** | [**\Briqpay\Models\SessionRequest**](../Model/SessionRequest.md)| Mandatory method to create a payment session | [optional]

### Return type

[**\Briqpay\Models\Session**](../Model/Session.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `patchCheckoutSession()`

```php
patchCheckoutSession($patch_session_request)
```

Patch a session

Use this method if you only want to update references and urls of a session

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = Briqpay\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$patch_session_request = new \Briqpay\Models\PatchSessionRequest(); // \Briqpay\Models\PatchSessionRequest

try {
    $apiInstance->patchCheckoutSession($patch_session_request);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->patchCheckoutSession: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **patch_session_request** | [**\Briqpay\Models\PatchSessionRequest**](../Model/PatchSessionRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `purchaseDecision()`

```php
purchaseDecision($sessionid, $purchase_decision_request)
```

Approve or deny a purchase a session

This method can be used for a last minute validation of the order.  If needed, request this functionality from your Briqpay representative

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = Briqpay\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$sessionid = fserf-tdghraikpnb-aefikpns-noliknef; // string | Session to be decided on
$purchase_decision_request = new \Briqpay\Models\PurchaseDecisionRequest(); // \Briqpay\Models\PurchaseDecisionRequest

try {
    $apiInstance->purchaseDecision($sessionid, $purchase_decision_request);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->purchaseDecision: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sessionid** | **string**| Session to be decided on |
 **purchase_decision_request** | [**\Briqpay\Models\PurchaseDecisionRequest**](../Model/PurchaseDecisionRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readRecurringToken()`

```php
readRecurringToken($read_recurring_token_request): \Briqpay\Models\ReadRecurringToken
```

Read a recurring token

This method allows you to read out all data on a recurring token, including completed payments

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = Briqpay\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$read_recurring_token_request = new \Briqpay\Models\ReadRecurringTokenRequest(); // \Briqpay\Models\ReadRecurringTokenRequest

try {
    $result = $apiInstance->readRecurringToken($read_recurring_token_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->readRecurringToken: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **read_recurring_token_request** | [**\Briqpay\Models\ReadRecurringTokenRequest**](../Model/ReadRecurringTokenRequest.md)|  | [optional]

### Return type

[**\Briqpay\Models\ReadRecurringToken**](../Model/ReadRecurringToken.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `readSession()`

```php
readSession($get_session_request): \Briqpay\Models\Session
```

Read session

After a completed purchase you can read the session in order to access the customer details.  At this point briqpay will provide you with company identification numbers as well as billing and shipping details.

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = Briqpay\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$get_session_request = new \Briqpay\Models\GetSessionRequest(); // \Briqpay\Models\GetSessionRequest | Read an existing session

try {
    $result = $apiInstance->readSession($get_session_request);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->readSession: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **get_session_request** | [**\Briqpay\Models\GetSessionRequest**](../Model/GetSessionRequest.md)| Read an existing session | [optional]

### Return type

[**\Briqpay\Models\Session**](../Model/Session.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: `application/json`

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `updateSession()`

```php
updateSession($update_session_request)
```

Update a session

If you want to update an ongoing session you can post to the ``/update`` endpoint using the correct Bearer Token for the session you wish to update This is valuable if you offer the ability to change cart values of freight options on the checkout page.  All variables are required. If you want to remove values, you should send in empty strings. If there are values you do not want to update, you should send in its existing value

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');


// Configure Bearer (JWT) authorization: bearerAuth
$config = Briqpay\Configuration::getDefaultConfiguration()->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$update_session_request = new \Briqpay\Models\UpdateSessionRequest(); // \Briqpay\Models\UpdateSessionRequest

try {
    $apiInstance->updateSession($update_session_request);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->updateSession: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **update_session_request** | [**\Briqpay\Models\UpdateSessionRequest**](../Model/UpdateSessionRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)

## `yourchargewebhookPost()`

```php
yourchargewebhookPost($recurring_charge_result_request)
```

Charge result

Briqpay will send a webhook for all succesfull, attempted, and failed recurring charges

### Example

```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');



$apiInstance = new Briqpay\Api\CheckoutApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$recurring_charge_result_request = new \Briqpay\Models\RecurringChargeResultRequest(); // \Briqpay\Models\RecurringChargeResultRequest

try {
    $apiInstance->yourchargewebhookPost($recurring_charge_result_request);
} catch (Exception $e) {
    echo 'Exception when calling CheckoutApi->yourchargewebhookPost: ', $e->getMessage(), PHP_EOL;
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **recurring_charge_result_request** | [**\Briqpay\Models\RecurringChargeResultRequest**](../Model/RecurringChargeResultRequest.md)|  | [optional]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: `application/json`
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#endpoints)
[[Back to Model list]](../../README.md#models)
[[Back to README]](../../README.md)
