# # LockFieldsConfiguration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**companyname** | [**\Briqpay\Models\LockType**](LockType.md) |  | [optional]
**firstname** | [**\Briqpay\Models\LockType**](LockType.md) |  | [optional]
**lastname** | [**\Briqpay\Models\LockType**](LockType.md) |  | [optional]
**streetaddress** | [**\Briqpay\Models\LockType**](LockType.md) |  | [optional]
**streetaddress2** | [**\Briqpay\Models\LockType**](LockType.md) |  | [optional]
**zip** | [**\Briqpay\Models\LockType**](LockType.md) |  | [optional]
**city** | [**\Briqpay\Models\LockType**](LockType.md) |  | [optional]
**cellno** | [**\Briqpay\Models\LockType**](LockType.md) |  | [optional]
**email** | [**\Briqpay\Models\LockType**](LockType.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
