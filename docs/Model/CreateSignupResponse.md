# # CreateSignupResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createddate** | **string** | Timestamp of when the registration started | [optional]
**locale** | **string** |  | [optional] [default to 'sv-se']
**country** | **string** |  | [optional] [default to 'SE']
**sessionid** | **string** |  | [optional]
**snippet** | **string** | the snippet to be presented in your frontend | [optional]
**merchanturls** | [**\Briqpay\Models\ReadSignupResponseMerchanturls**](ReadSignupResponseMerchanturls.md) |  | [optional]
**state** | **string** |  | [optional]
**merchantid** | **string** | Your merchant ID in briqpays system | [optional]
**merchantname** | **string** | Your merchant Name in briqpays system | [optional]
**validation_flow** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
