# # MerchantReference

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**reference1** | **string** |  | [optional]
**reference2** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
