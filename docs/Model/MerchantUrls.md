# # MerchantUrls

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**terms** | **string** | link to your terms that the client approves at the point of purchase |
**notifications** | **string** | Endpoint for briqpay to notify you of a successfull purchase - ensure to handle different values in session.state |
**redirecturl** | **string** | Where should the customer be redirected after the purchase has been completed? |
**backtocheckout** | **string** | Used for redirected payment methods when customer clicks &#39;back to store&#39; links before completing the purchase - Should be a link to your checkout page | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
