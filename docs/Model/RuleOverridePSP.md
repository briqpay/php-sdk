# # RuleOverridePSP

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**psp_id** | **string** | the PSPid of the payment method that you wish to manually controll | [optional]
**handles** | [**\Briqpay\Models\RuleOverridePSPHandles**](RuleOverridePSPHandles.md) |  | [optional]
**other_handles** | **string** | How should the other handles that are configured on this purchase act? | [optional] [default to 'run_rules']

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
