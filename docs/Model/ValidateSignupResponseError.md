# # ValidateSignupResponseError

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error_code** | **string** | Error code to explain the failed validation. Will be used to display information to the user |
**error_field** | **string** | What field this error applies to |
**error_message** | **string** | Additional error data - will not be displayed to the user | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
