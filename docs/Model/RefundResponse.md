# # RefundResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refundid** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
