# # RecurringChargeResultRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**planid** | **string** | Planid of the recurring charge | [optional]
**recurring_token** | **string** | The recurring token of the attempt | [optional]
**sessionid** | **string** | The sessionid of the successfull charge, will only be present if state &#x3D;&#x3D;&#x3D; sucess | [optional]
**state** | **string** | The state of the charge. If state &#x3D;&#x3D;&#x3D; attempt, briqpay will attempt to retry according to your defined retrypolicy. If state &#x3D;&#x3D;&#x3D; abort, the subscription is deemed to be voided | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
