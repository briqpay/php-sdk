# # ReadSignupResponseAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**companyname** | **string** |  | [optional]
**streetaddress** | **string** |  | [optional]
**zip** | **string** |  | [optional]
**city** | **string** |  | [optional]
**hq** | **bool** | Wheter or not the selected address is the registered HQ of the company | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
