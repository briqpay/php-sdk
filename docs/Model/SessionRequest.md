# # SessionRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | **string** |  | [default to 'SEK']
**locale** | **string** |  | [default to 'sv-se']
**country** | **string** |  | [default to 'SE']
**amount** | **int** | The total amount of the order including VAT |
**recurring** | [**\Briqpay\Models\SessionRequestRecurring**](SessionRequestRecurring.md) |  | [optional]
**cart** | [**\Briqpay\Models\CartItem[]**](CartItem.md) |  |
**merchanturls** | [**\Briqpay\Models\MerchantUrls**](MerchantUrls.md) |  |
**merchantconfig** | [**\Briqpay\Models\MerchantConfig**](MerchantConfig.md) |  | [optional]
**reference** | [**\Briqpay\Models\MerchantReference**](MerchantReference.md) |  | [optional]
**orgnr** | **string** |  | [optional]
**billingaddress** | [**\Briqpay\Models\Address**](Address.md) |  | [optional]
**shippingaddress** | [**\Briqpay\Models\Address**](Address.md) |  | [optional]
**merchant_billing** | [**\Briqpay\Models\SessionRequestMerchantBilling**](SessionRequestMerchantBilling.md) |  | [optional]
**merchant_shipping** | [**\Briqpay\Models\SessionRequestMerchantShipping**](SessionRequestMerchantShipping.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
