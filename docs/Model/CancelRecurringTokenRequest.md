# # CancelRecurringTokenRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**recurring_token** | **string** | The recurring token you wish to cancel | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
