# # PaymentConfigurationPspRulesOverride

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**psp** | [**\Briqpay\Models\RuleOverridePSP[]**](RuleOverridePSP.md) |  | [optional]
**other_psps** | **string** | How should the rule-engine act for remaining PSPs that you have not defined an override for? | [optional] [default to 'run_rules']

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
