# # ReadRecurringToken

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**payment_methods** | [**\Briqpay\Models\CartItem[]**](CartItem.md) |  | [optional]
**initial_cart** | [**\Briqpay\Models\CartItem[]**](CartItem.md) |  | [optional]
**state** | **string** | Current state of the subscription | [optional]
**merchantid** | **string** | Your merchant ID withing Briqpay system | [optional]
**planid** | **string** | The unique plan id for this subscription | [optional]
**initial_session_id** | **string** | The intial session that created this recurring token | [optional]
**sessions** | [**\Briqpay\Models\RecurringSession[]**](RecurringSession.md) | All sessions that this recurring token has created | [optional]
**recurring_token** | **string** | The recurring token ID | [optional]
**billingaddress** | [**\Briqpay\Models\Address**](Address.md) |  | [optional]
**active_psp_id** | **string** | The currently active pspID for this subscription | [optional]
**created_at** | **string** | The date this subscription was created | [optional]
**updated_at** | **string** | Last time this subscription was updated | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
