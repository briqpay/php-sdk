# # CreateCaptureRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sessionid** | **string** |  | [optional]
**amount** | **int** | The total amount of the capture including VAT | [optional]
**cart** | [**\Briqpay\Models\CartItem[]**](CartItem.md) | The array of cart items to be captured | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
