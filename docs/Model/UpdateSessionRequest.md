# # UpdateSessionRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | **string** |  | [default to 'SEK']
**locale** | **string** |  | [default to 'sv-se']
**country** | **string** |  | [default to 'SE']
**sessionid** | **string** | The sessionid to be patched |
**cart** | [**\Briqpay\Models\CartItem[]**](CartItem.md) |  |
**merchanturls** | [**\Briqpay\Models\MerchantUrls**](MerchantUrls.md) |  |
**amount** | **int** | The total amount of the order including VAT |
**reference** | [**\Briqpay\Models\MerchantReference**](MerchantReference.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
