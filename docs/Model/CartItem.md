# # CartItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**producttype** | [**\Briqpay\Models\ProductType**](ProductType.md) |  | [optional]
**reference** | **string** | The SKU of the item | [optional]
**name** | **string** |  | [optional]
**quantity** | **int** |  | [optional]
**quantityunit** | **string** |  | [optional]
**unitprice** | **int** | Unit price exckluding VAT | [optional]
**taxrate** | **int** | The applicable taxrate in minor units. Eg 25% &#x3D; 2500 | [optional]
**discount** | **int** | Discount value in percentages. 10% &#x3D; 1000 | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
