# # ReadSignupResponseMerchanturls

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**terms** | **string** |  | [optional]
**signupwebhook** | **string** | The url where briqpay should post data for you to validate the registration | [optional]
**redirecturl** | **string** | The page where briqpay should redirect the customer after a successful registration | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
