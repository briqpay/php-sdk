# # CreateSignupRequestMerchanturls

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**terms** | **string** |  | [optional]
**signupwebhook** | **string** | If you want to validate the signup from your backend. you should provide briqpay with this url for us to notify you of a completed registration.  This validation is also possible to complete using a frontend notification. | [optional]
**redirecturl** | **string** | If you want the signup to automatically redirect after completion | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
