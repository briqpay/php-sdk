# # MerchantPrefillAddress

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**streetaddress** | **string** |  | [optional]
**streetaddress2** | **string** |  | [optional]
**zip** | **string** |  | [optional]
**city** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
