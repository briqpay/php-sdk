# # ValidateSignupResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**result** | **bool** | true/false depending on the validation result |
**errors** | [**\Briqpay\Models\ValidateSignupResponseError[]**](ValidateSignupResponseError.md) | Errors to display to the user as to why the registration failed | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
