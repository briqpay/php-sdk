# # IndividualRuleResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**outcome** | **bool** |  | [optional]
**friendlyname** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
