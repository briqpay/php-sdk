# # BriqpayCreateHPPSessionRequestDeliverymethod

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **string** | Can be value of &#39;email&#39;, &#39;link&#39; or &#39;sms&#39; |
**destination** | **string** | The destination of the hostedpage should be email-address or cellphone number |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
