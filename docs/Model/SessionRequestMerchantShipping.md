# # SessionRequestMerchantShipping

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**addresses** | [**\Briqpay\Models\MerchantPrefillAddress[]**](MerchantPrefillAddress.md) | A list of pre-approved addresses that you wish your customers should be able to select as shipping address for this specific purchase | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
