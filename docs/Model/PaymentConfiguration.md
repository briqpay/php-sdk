# # PaymentConfiguration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**psp_rules_override** | [**\Briqpay\Models\PaymentConfigurationPspRulesOverride**](PaymentConfigurationPspRulesOverride.md) |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
