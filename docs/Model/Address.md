# # Address

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**companyname** | **string** |  | [optional]
**firstname** | **string** |  | [optional]
**lastname** | **string** |  | [optional]
**streetaddress** | **string** |  | [optional]
**streetaddress2** | **string** |  | [optional]
**zip** | **string** |  | [optional]
**city** | **string** |  | [optional]
**cellno** | **string** |  | [optional]
**email** | **string** |  | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
