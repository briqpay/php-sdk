# # Session

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currency** | **string** |  | [optional] [default to 'SEK']
**locale** | **string** |  | [optional] [default to 'sv-se']
**country** | **string** |  | [optional] [default to 'SE']
**orgnr** | **string** |  | [optional]
**billingaddress** | [**\Briqpay\Models\Address**](Address.md) |  | [optional]
**shippingaddress** | [**\Briqpay\Models\Address**](Address.md) |  | [optional]
**reference** | [**\Briqpay\Models\MerchantReference**](MerchantReference.md) |  | [optional]
**cart** | [**\Briqpay\Models\CartItem[]**](CartItem.md) |  | [optional]
**merchanturls** | [**\Briqpay\Models\MerchantUrls**](MerchantUrls.md) |  | [optional]
**amount** | **int** | The total amount of the order including VAT | [optional]
**snippet** | **string** |  | [optional]
**tags** | [**\Briqpay\Models\SessionTags**](SessionTags.md) |  | [optional]
**sessionid** | **string** | The unique identifier for this specific session | [optional]
**state** | [**\Briqpay\Models\SessionState**](SessionState.md) |  | [optional]
**createddate** | **\DateTime** |  | [optional]
**ordernote** | [**\Briqpay\Models\OrderNote**](OrderNote.md) |  | [optional]
**purchasepaymentmethod** | [**\Briqpay\Models\SessionPurchasepaymentmethod**](SessionPurchasepaymentmethod.md) |  | [optional]
**extra_company_fields** | **array<string,string>** | Record of custom parameters that briqpay will share on completed sessions - Contact briqpay for complete specifications | [optional]
**rulesresult** | [**\Briqpay\Models\PSPRuleResults[]**](PSPRuleResults.md) | Get detailed information on what rules triggered for this purchase | [optional]
**token** | **string** | The Bearer token to use for all subsequent calls on this session - This is only return on Session Create Calls | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
