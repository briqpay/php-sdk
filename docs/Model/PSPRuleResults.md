# # PSPRuleResults

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pspname** | **string** | What PSP was triggered | [optional]
**rules_result** | [**\Briqpay\Models\IndividualRuleResult[]**](IndividualRuleResult.md) | All rules that failed for this method | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
