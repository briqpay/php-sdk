# # OrderNoteConfiguration

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**custom_inputs** | [**\Briqpay\Models\CustomInput[]**](CustomInput.md) | Avaliable custom inputs fields that should be collected in the order_note section of the checkout | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
