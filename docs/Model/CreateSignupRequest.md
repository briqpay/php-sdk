# # CreateSignupRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | **string** |  |
**locale** | **string** |  |
**cin** | **string** | Company identication number | [optional]
**merchanturls** | [**\Briqpay\Models\CreateSignupRequestMerchanturls**](CreateSignupRequestMerchanturls.md) |  |

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
