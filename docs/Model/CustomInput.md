# # CustomInput

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**key** | **string** | The input key, must be unique between all custominputs | [optional]
**label** | **string** | The label to be presented in relation to the input | [optional]
**min_length** | **float** |  | [optional]
**max_length** | **float** |  | [optional]
**required** | **bool** |  | [optional] [default to false]
**span** | **float** | Control if the input field should span over 1 or 2 columns | [optional]

[[Back to Model list]](../../README.md#models) [[Back to API list]](../../README.md#endpoints) [[Back to README]](../../README.md)
